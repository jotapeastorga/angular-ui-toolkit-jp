angular.module('uk-toolkit.components')
    .directive('ukNumberInput', function($filter, $Localization) {
        return {
            restrict: 'E',
            require: ['ukNumberInput', 'ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                minValue: '@',
                maxValue: '@',
                maxLength: '@',
                format: '=?',
                errorResource: '@',
                ngDisabled:'=',
                ngChange:'&'
            },
            transclude: false,
            templateUrl: 'uk-number-input/uk-number-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                var ngModel = ctrls[1];
                var vm = scope.vm;

                // do nothing if no ng-model
                if (!ngModel) {
                    return;
                }

                // ------------------------------
                // Event's
                // ------------------------------
                scope.onFocus = function() {
                    //$element.addClass(focusActiveClass);
                    if (vm.isValidityValid) {
                        vm.viewValue = scope.ngModel;
                    }
                };
                scope.onBlur = function() {
                    //$element.removeClass(focusActiveClass);
                    ctrl.humanizeViewValue();
                };
                scope.onChange = function(value) {
                    ngModel.$setViewValue(value);
                     if(scope.ngChange){
                     scope.ngChange();   
                    }
                };

                // ------------------------------
                // ngModel Methods
                // ------------------------------
                // This functions only "call" when ngModel changes from outside ^^ the directive, 
                // soooo before we put that value we try to parse and format as expected
                // IN RESUME, THIS FORMATTERS ONLY CALL IF THE NGMODEL CHANGE FROM
                // OUTSIDE THE DIRECTIVE :P!
                ngModel.$formatters.push(function(inputValue) {
                    //always set the vm.viewValue
                    vm.viewValue = inputValue;
                    ngModel.$setViewValue(inputValue); //Init $parser and $validator process
                    ctrl.humanizeViewValue();

                    return inputValue;
                });

                // Add the parser , to ensure the model will be the "real model value"
                // CALL FIRST, if fails, all stop, and set invalid the "directive"
                ngModel.$parsers.push(ctrl.tryToParseViewValue);

                // Add the validators (call always , but wee only change the value programaticaly)
                // IF PARSER IS SUCCESSFULLY , CALL THE VALIDATORS 
                // (can be a multiple validations :P)
                ngModel.$validators.isNumber = function(modelValue, viewValue) {
                    return vm.isValidityValid;
                };
            },
            controller: function($scope, $element, $q) {
                var ctrl = this;

                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    isValidityValid: true
                };

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------

                // VALIDATOR: Check if the value is Number
                this.isNumber = function(rawValue) {
                    var value = rawValue;

                    //VALIDATE IF IS NUMBER
                    if (!value || (value && value.length === 0)) {
                        vm.isValidityValid = true;
                        return vm.isValidityValid;
                    }
                    var isNumber = (/^-?\d+$/.test(value)) ? true : undefined;

                    vm.isValidityValid = isNumber; //set for better perfomance
                    return vm.isValidityValid;
                };

                // VALIDATOR: Check if the value is in Range
                this.isInRange = function(rawValue) {
                    //Assume number, because isNumber called first
                    var numberValue = parseInt(rawValue);

                    var maxValue = parseInt($scope.maxValue);
                    var minValue = parseInt($scope.minValue);

                    maxValue = isNaN(maxValue) ? Number.MAX_VALUE : maxValue;
                    minValue = isNaN(minValue) ? -(Math.pow(2, 53) - 1) : minValue;

                    var isInMinRange = true;
                    var isInMaxRange = true;

                    if (numberValue < minValue) {
                        isInMinRange = false;
                    }

                    if (numberValue > maxValue) {
                        isInMaxRange = false;
                    }

                    if (!isInMinRange || !isInMaxRange) {
                        vm.isValidityValid = false; //not in range
                    }

                    return vm.isValidityValid;
                };

                // ------------------------------
                // Try to parse the Raw Value to "Model Value"
                // (if fails the pipeline is cancelled and set invalid)
                // ------------------------------
                this.tryToParseViewValue = function(rawViewValue) {
                    if (ctrl.isNumber(rawViewValue) && ctrl.isInRange(rawViewValue)) {
                        var modelValue = parseInt(rawViewValue);
                        return modelValue;
                    }
                };

                // ------------------------------
                // Parse the Model Value to a "Friendly Value"
                // for the user
                // ------------------------------
                this.humanizeViewValue = function() {
                    if (vm.isValidityValid) {
                        if ($scope.format === true) {
                            vm.viewValue = $filter('number')($scope.ngModel, 0);
                        }
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                //Check character input is only number
                characterNumber = function(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false;
                    }
                    return true;
                };

                vm.isValid = function() {
                    return vm.isValidityValid;
                };
                vm.isFilled = function() {
                    return (vm.viewValue || '').toString().length > 0;
                };
            }
        };
    });