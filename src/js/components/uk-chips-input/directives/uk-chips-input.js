angular.module('uk-toolkit.components')
    .directive('ukChipsInput', function($compile) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@', //Place Holder for the input
                onSearch: '&', //When Call to Search
                onItemSelected: '&', //Last chance to modify the model
                itemText: '@', // Text Value Display
                ngBlur: '&', //Call when the user out of the focus (BLUR)
                minLengthForSearch: '@', // Minimum size of characters for start search
                delayTimeForSearch: '@' //Delay Time for start search (milliseconds)
            },
            transclude: false,
            templateUrl: 'uk-chips-input/uk-chips-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.chips = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: function($scope, $element, $q, $timeout) {
                var container = $element.find("uk-chips-input-container");
                var autoCompleteContainer = $element.find("uk-input-autocomplete");
                var resultList = autoCompleteContainer.find("uk-dropdown");
                var selectedItems = container.find("uk-chips-wrap");

                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true
                };

                try{
                    $scope.minLengthForSearch = parseInt($scope.minLengthForSearch); 
                }catch(error){
                    $scope.minLengthForSearch = 3;
                }
                

                try{
                    $scope.delayTimeForSearch = parseInt($scope.delayTimeForSearch);
                }catch(error){                    
                    $scope.delayTimeForSearch = 650; 
                }
                

                //---------------------------------------------------
                // Build the selected chips, by his template!
                var template = null;
                if ($scope.template) {
                    template = $scope.template;
                } else {
                    template = '{{item.' + $scope.itemText + '}}';
                }

                var fragment = [
                    '<uk-chips-selecteds class="tc-text-light">',
                    '   <uk-chip ng-repeat="item in ngModel" ng-class="{\'for-delete\': item.forDelete}">',
                    '       <uk-chip-content>',
                    template,
                    '       </uk-chip-content>',
                    '       <uk-chip-remove ng-click="vm.removeFromModel($index)">',
                    '           <div class="uk-icon-close">',
                    '           </div>',
                    '       </uk-chip-remove>',
                    '   </uk-chip>',
                    '</uk-chips-selecteds>'
                ].join("");

                //First list
                selectedItems.html("");
                selectedItems.append($compile(fragment)($scope));
                //---------------------------------------------------


                // ------------------------------
                // Private Method's
                // ------------------------------
                var AddToModel = function(item) {
                    var handler = $scope.onItemSelected();
                    if (handler) {
                        handler(item).then(function(newItem) {
                            item = newItem;
                        });
                    }


                    //Find if exists =), to check unique (only for item-text,
                    // otherwise, they need to do in the "on-selected-item" event)!
                    if ($scope.ngModel && item) {
                        var customFilter = {};
                        customFilter[$scope.itemText] = item[$scope.itemText];

                        var exists = _.find($scope.ngModel, customFilter);
                        if (exists) {
                            //Nullify item to skip the add phase to Model
                            item = null;
                        }
                    }

                    if (item) {
                        if (!$scope.ngModel) {
                            $scope.ngModel = [];
                        }

                        $scope.ngModel.push(item);

                        vm.isValidityValid = true;
                    }

                    //Reset the counters!
                    delete vm.resultCollection; //Result Collection for Autocomplete
                    delete vm.selectedResultCollection;
                    vm.viewValue = "";
                    autoCompleteContainer.removeClass("uk-open");
                };

                var clearDeletionMarker = function() {
                    //Remove the last for-delete (special UI marker)
                    if ($scope.ngModel && $scope.ngModel.length > 0) {
                        delete $scope.ngModel[$scope.ngModel.length - 1].forDelete;
                    }
                };
                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.removeFromModel = function(index) {
                    if ($scope.ngModel) {
                        $scope.ngModel.splice(index, 1);

                        if ($scope.ngModel.length === 0) {
                            $scope.ngModel = null;
                            vm.isValidityValid = true;
                        }
                    }
                };
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    return ($scope.ngModel && $scope.ngModel.length > 0);
                };

                // ------------------------------
                // Event's
                // ------------------------------
                vm.onKeyDown = function(ev) {
                    switch (ev.keyCode) {
                        case 8: //DELETE
                            if (vm.viewValue === "" && $scope.ngModel && $scope.ngModel.length > 0) {
                                //get the last model!
                                var lastIndex = $scope.ngModel.length - 1;
                                var lastModel = $scope.ngModel[lastIndex];
                                if (lastModel.forDelete) {
                                    vm.removeFromModel(lastIndex);
                                } else {
                                    lastModel.forDelete = true;
                                }
                            }
                            break;
                    }
                };

                vm.onKeyUp = function(ev) {

                    switch (ev.keyCode) {
                        case 8:
                            //DO NOTHING (FOR NO-BLOCK KEYDOWN)
                            break;
                        case 13: //ENTER
                            if (vm.resultCollection) {
                                vm.selectResultItem(vm.selectedResultCollection);
                            } else {
                                //If on-search is empty, means, free text
                                var handler = $scope.onSearch();
                                if (!handler) {
                                    AddToModel({
                                        name: vm.viewValue
                                    });
                                }
                            }
                            clearDeletionMarker();
                            break;
                        case 38: //UP ARROW
                            if (vm.resultCollection) {
                                vm.selectedResultCollection--;
                                if (vm.selectedResultCollection < 0) {
                                    vm.selectedResultCollection = vm.resultCollection.length - 1;
                                }
                            }
                            clearDeletionMarker();
                            break;
                        case 40: //DOWN ARROW
                            if (vm.resultCollection) {
                                vm.selectedResultCollection++;
                                if (vm.selectedResultCollection >= vm.resultCollection.length) {
                                    vm.selectedResultCollection = 0;
                                }
                            }
                            clearDeletionMarker();
                            break;
                        default:
                            clearDeletionMarker();
                            break;
                    }
                };
                vm.selectResultItem = function(index) {
                    if (vm.resultCollection) {
                        var selected = vm.resultCollection[index];
                        AddToModel(selected);
                    }
                };
                vm.onResultsItemHover = function(ev, index) {
                    if (vm.resultCollection) {
                        vm.selectedResultCollection = index;
                    }
                };
                vm.onFocus = function() {
                    container.addClass("uk-input-focus");
                };
                vm.onBlur = function() {
                    container.removeClass("uk-input-focus");
                    clearDeletionMarker();
                    var handler = $scope.ngBlur();
                    if (handler) {
                        handler();
                    }
                    // AA: Se limpia el input de busqueda
                    vm.viewValue = null;
                    // AA: Se deja un delay para cerrar la lista de items para no chocar con un posible onClick
                    var delay = $timeout(function(){
                        autoCompleteContainer.removeClass("uk-open");
                        },120);
                };
                var debounced = null;
                vm.onChange = function(value) {
                    if (value && value.length >= parseInt($scope.minLengthForSearch)) {
                        if (!debounced) {
                            debounced = _.debounce(function() {
                                var handler = $scope.onSearch();
                                if (handler) {
                                    var defer = handler(vm.viewValue);
                                    defer.then(function(results) {
                                        
                                        // AA: Si no se encuentran resultados no debe pintar nada
                                        if(results.length === 0){
                                            autoCompleteContainer.removeClass("uk-open");
                                            return;
                                        }

                                        //---------------------------------------------------
                                        // Bind Template Html
                                        var template = null;
                                        if ($scope.template) {
                                            template = $scope.template;
                                        } else {
                                            template = '<a>{{item.' + $scope.itemText + '}}</a>';
                                        }

                                        var fragment = [
                                            '<ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results">',
                                            "<li ng-repeat='item in vm.resultCollection' ng-click='vm.selectResultItem($index)' ng-mouseover='vm.onResultsItemHover($event,$index)' ng-class='{\"uk-active\":vm.selectedResultCollection == $index}'>",
                                            template,
                                            '</li>',
                                            '</ul>'
                                        ].join("");

                                        //First list
                                        vm.resultCollection = results;
                                        vm.selectedResultCollection = 0;
                                        resultList.html("");
                                        resultList.append($compile(fragment)($scope));
                                        //---------------------------------------------------

                                        autoCompleteContainer.addClass("uk-open");
                                    });
                                } else {
                                    //Add directly to Model
                                    $scope.itemText = "name"; //set the name
                                }
                            }, parseInt($scope.delayTimeForSearch));
                        }
                        debounced();

                    } else {
                        if (debounced) {
                            debounced.cancel();
                            debounced = null;
                        }
                        delete vm.resultCollection; //Result Collection for Autocomplete
                        delete vm.selectedResultCollection;
                        autoCompleteContainer.removeClass("uk-open");
                    }
                };
            }
        };
    });
