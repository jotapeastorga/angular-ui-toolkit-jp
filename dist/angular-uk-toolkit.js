/*------------------------------------------------------
 Company:           Seguros Falabella
 Author:            Célula 339 <celula339@falabella.cl> (https://bitbucket.org/segurosfalabella/angular-uk-toolkit)
 
 Description:       Falabella Angular UI Toolkit
 Github:            https://bitbucket.org/segurosfalabella/angular-uk-toolkit

 Versión:           1.0.21
 Build Date:        2017-06-29 11:15:10
------------------------------------------------------*/

angular.module('uk-toolkit.templates', []).run(['$templateCache', function ($templateCache) {
  "use strict";
  $templateCache.put("uk-calendar/uk-calendar.tpl.html",
    "<div class=uk-datepicker><div class=uk-datepicker-nav><a href=\"\" ng-click=vm.previousMonth() class=uk-datepicker-previous></a> <a href=\"\" ng-click=vm.nextMonth() class=uk-datepicker-next></a><div class=uk-datepicker-heading><span class=uk-form-select>{{vm.monthNames[vm.currentDate.getMonth()]}}&nbsp;<select class=update-picker-month ng-change=vm.setMonth(vm.pickerMonth) ng-model=vm.pickerMonth><option ng-value={{$index}} ng-selected=\"$index == vm.currentDate.getMonth()\" ng-repeat=\"month in vm.monthNames\">{{month.substring(0,1).toUpperCase() + month.substring(1)}}</option></select></span><span class=uk-form-select>{{vm.currentDate.getFullYear()}}<select class=update-picker-year ng-change=vm.setYear(vm.pickerYear) ng-model=vm.pickerYear><option ng-value={{year}} ng-selected=\"year == vm.currentDate.getFullYear()\" ng-repeat=\"year in vm.years\">{{year}}</option></select></span></div></div><table class=uk-datepicker-table ng-if=vm.dayNames><thead><tr><th>{{vm.dayNames[1].substring(0,3)}}</th><th>{{vm.dayNames[2].substring(0,3)}}</th><th>{{vm.dayNames[3].substring(0,3)}}</th><th>{{vm.dayNames[4].substring(0,3)}}</th><th>{{vm.dayNames[5].substring(0,3)}}</th><th>{{vm.dayNames[6].substring(0,3)}}</th><th>{{vm.dayNames[0].substring(0,3)}}</th></tr></thead><tbody><tr ng-repeat=\"week in vm.currentMonthWeek\"><td ng-repeat=\"day in week\"><a href=\"\" ng-click=vm.selectDate(day.date,day.muted) ng-class=\"{'uk-datepicker-table-muted': day.muted, 'uk-active': (day.selected && !day.muted)}\">{{day.date.getDate()}}</a></td></tr></tbody></table></div>");
  $templateCache.put("uk-chips-input/uk-chips-input.tpl.html",
    "<uk-chips-input-container class=\"material-input input tc-chips\" ng-class=\"{'uk-input-error': !vm.isValid(), 'filled': vm.isFilled()}\"><div><uk-chips-wrap></uk-chips-wrap><uk-input-autocomplete class=\"uk-autocomplete uk-form\"><input id={{vm.identifier}} ng-focus=vm.onFocus() ng-keyup=vm.onKeyUp($event) ng-keydown=vm.onKeyDown($event) ng-model=vm.viewValue ng-change=vm.onChange(vm.viewValue) ng-blur=vm.onBlur()><uk-dropdown class=\"uk-dropdown tc-dropdown\" aria-expanded=false></uk-dropdown></uk-input-autocomplete></div><span class=material-bar></span><label for={{vm.identifier}}>{{placeholder}}</label></uk-chips-input-container>");
  $templateCache.put("uk-datepicker-input/uk-datepicker-input.tpl.html",
    "<material-input class=\"material-input input\" ng-class=\"{'uk-input-error': !vm.isValid() }\"><input id={{vm.identifier}} ng-click=vm.cancelPropagation($event) ng-model=vm.viewValue ng-change=vm.onChange(vm.viewValue) ng-focus=vm.onFocus() ng-blur=vm.onBlur() ng-class=\"{'filled': vm.isFilled() || vm.isOnDropDown}\"><uk-dropdown class=uk-dropdown aria-expanded=false ng-click=vm.cancelPropagation($event)><uk-calendar ng-model=ngModel on-change-date=vm.onChangeDate min-date=vm.minDate></uk-calendar></uk-dropdown><span class=material-bar></span><label for={{vm.identifier}}>{{placeholder}}</label></material-input>");
  $templateCache.put("uk-email-input/uk-email-input.tpl.html",
    "<div class=\"material-input input\" ng-class=\"{'uk-input-error': !vm.isValid()}\"><input id={{vm.identifier}} maxlength={{maxLength}} ng-model=vm.viewValue ng-change=vm.onChange(vm.viewValue) ng-focus=vm.onFocus() ng-blur=vm.onBlur() ng-class=\"{'filled': vm.isFilled()}\"> <span class=material-bar></span><label for={{vm.identifier}}>{{placeholder}}</label></div><small class=\"error filled uk-align-left\" ng-if=\"!vm.isValid() && vm.errorLabel\">{{vm.errorLabel}}</small>");
  $templateCache.put("uk-loading/uk-loading.tpl.html",
    "<div class=spinner></div><div class=\"uk-h5 uk-text-center lr-loading tc-text-light\">{{legend}}</div>");
  $templateCache.put("uk-number-input/uk-number-input.tpl.html",
    "<div class=\"material-input input\" ng-class=\"{'uk-input-error': !vm.isValid()}\"><input id={{vm.identifier}} maxlength={{maxLength}} ng-model=vm.viewValue ng-change=onChange(vm.viewValue) onkeypress=\"return characterNumber(event)\" ng-focus=onFocus() ng-blur=onBlur() ng-class=\"{'filled': vm.isFilled()}\" ng-disabled=ngDisabled> <span class=material-bar></span><label for={{vm.identifier}}>{{placeholder}}</label></div><small class=\"error filled uk-align-left\" ng-if=\"!vm.isValid() && vm.errorLabel\">{{vm.errorLabel}}</small>");
  $templateCache.put("uk-option-list/uk-option-list.tpl.html",
    "<div><label>{{placeholder}}</label><uk-option-items class=\"uk-grid uk-grid-small uk-margin-top\" ng-if=vm.template option-change=vm.optionChange option-template={{vm.template}} item-collection=itemCollection ng-model=ngModel></uk-option-items></div><option-template ng-transclude></option-template>");
  $templateCache.put("uk-phone-input/uk-phone-input.tpl.html",
    "<div class=\"material-input input\" ng-class=\"{'uk-input-error': !vm.isValid()}\"><input maxlength={{vm.phoneLength}} class=uk-input-prefix id={{vm.identifier}} ng-model=vm.viewValue onkeypress=\"return characterNumber(event)\" ng-keyup=vm.onKeyUp($event,this) ng-change=vm.onChange(vm.viewValue) ng-focus=vm.onFocus() ng-blur=vm.onBlur() ng-class=\"{'filled': vm.isFilled()}\"> <span class=material-bar></span><label for={{vm.identifier}}>{{placeholder}}</label><i class=uk-i-prefix>{{country}}</i></div><small class=\"error filled uk-align-left\" ng-if=\"!vm.isValid() && vm.errorLabel\">{{vm.errorLabel}}</small>");
  $templateCache.put("uk-rut-input/uk-rut-input.tpl.html",
    "<div class=\"material-input input\" ng-class=\"{'uk-input-error': !vm.isValid()}\"><input maxlength={{vm.rutLength}} id={{vm.identifier}} ng-model=vm.viewValue ng-change=vm.onChange(vm.viewValue) ng-focus=vm.onFocus() ng-blur=vm.onBlur() ng-class=\"{'filled': vm.isFilled()}\"> <span class=material-bar></span><label for={{vm.identifier}}>{{placeholder}}</label></div><small class=\"error filled uk-align-left\" ng-if=\"!vm.isValid() && vm.errorLabel\">{{vm.errorLabel}}</small>");
  $templateCache.put("uk-select-input/uk-select-input.tpl.html",
    "<div class=\"uk-form-select uk-width-1-1 uk-button mod-form-dropdown uk-button-large uk-text-left uk-active\"><small class=\"uk-display-block uk-width-1-1 ng-binding\">{{placeholder}}</small><select-item-text-value class=\"uk-width-1-1 lr-selected-label\"><span ng-if=vm.displayValue class=display-value-item>{{vm.displayValue}} </span><span ng-if=\"!vm.isLoading && !vm.displayValue\" class=empty-value-item>{{emptyItemText || '...'}} </span><span ng-if=vm.isLoading class=loading-text-item>{{loadingItemText}}</span></select-item-text-value><i ng-if=!vm.isLoading class=\"uk-align-right uk-icon-medium uk-icon uk-icon-arrow_down\"></i><div ng-if=vm.isLoading class=\"uk-align-right uk-icon-medium uk-icon spinner mini\"></div><select-container></select-container></div>");
  $templateCache.put("uk-text-input/uk-text-input.tpl.html",
    "<div class=\"material-input input\" ng-class=\"{'uk-input-error': !vm.isValid()}\"><input id={{vm.identifier}} ng-model=vm.viewValue maxlength={{maxLength}} ng-change=vm.onChange(vm.viewValue) ng-focus=vm.onFocus() ng-blur=vm.onBlur() ng-class=\"{'filled': vm.isFilled()}\" ng-model-options=\"{ allowInvalid: true}\" ng-disabled=ngDisabled> <span class=material-bar></span><label for={{vm.identifier}}>{{placeholder}}</label></div><div class=\"uk-align-right uk-icon-medium uk-icon spinner mini\" ng-if=isLoading style=\"margin: -45px 10px 0 0\"></div><small class=\"error filled uk-align-left\" ng-if=\"!vm.isValid() && vm.errorLabel\">{{vm.errorLabel}}</small>");
}]);
;angular.manifiest('uk-toolkit', [
    'uk-toolkit.templates',
    'uk-toolkit.components'
], [
    'gale'      //ANGULAR GALE CORE LIBRARY
]);;angular.module('uk-toolkit.components')
    .directive('ukCalendar', ['$filter', '$locale', function($filter, $locale) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                onChangeDate: '&',
                minDate : '='
            },
            transclude: false,
            templateUrl: 'uk-calendar/uk-calendar.tpl.html',

            link: function(scope, element, attrs, ctrls, ngModel) {
                //Validator Step
                var ctrl = ctrls[0];
                ctrl.$validators.isDate = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };

            scope.$watch('ngModel', function(newValue, oldValue) {
                
                if (newValue) {
                    scope.vm.setDate(newValue);
                }
            }, true);

            },
            controller: ['$scope', '$element', '$q', function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true
                };



                var parseDate = function(chosenDate) {
                    return new Date(chosenDate);
                };

                //static date (always set the last date)

                //Get Tomorrow Date
                var tomorrowDate = new Date();

                var chosenDate = tomorrowDate.toISOString();
         
                if ($scope.minDate) {
                    if (chosenDate < $scope.minDate) {
                        chosenDate = $scope.minDate;
                    }
                }
                chosenDate = parseDate(chosenDate);

                vm.currentDate = new Date(chosenDate);
                var getWeekMonths = function(date) {
                    var month = date.getMonth();
                    var year = date.getFullYear();

                    var from = new Date(year, month, 1);
                    var to = new Date(new Date(year, month + 1, 1) - 1);

                    //We need to start on Monday (1) and end's on Sunday (0)
                    var calendarFrom = from;
                    var calendarTo = to;
                    while (calendarFrom.getDay() !== 1) {
                        calendarFrom.setDate(calendarFrom.getDate() - 1);
                    }
                    while (calendarTo.getDay() !== 0) {
                        calendarTo.setDate(calendarTo.getDate() + 1);
                    }

                    //Now .... we need to split the days in groups of week's (7 days)
                    //so.. start to building the "week's"
                    var weeks = [];
                    var currentWeek = [];
                    var untilTime = calendarTo.getTime();
                    while (calendarFrom.getTime() < untilTime) {
                        var isSelected = false;
                        if (chosenDate.getDate() === calendarFrom.getDate() &&
                            chosenDate.getMonth() === calendarFrom.getMonth() &&
                            chosenDate.getFullYear() === calendarFrom.getFullYear()) {
                            isSelected = true;
                        }
                        var muted = null;
                        if($scope.minDate){
                           
                            var minDDateFormatted = moment($scope.minDate.toLocaleDateString(),'DD/MM/YYYY');
                            var currentDDateFormatted = moment(calendarFrom.toLocaleDateString(),'DD/MM/YYYY');
                            muted = (( minDDateFormatted > currentDDateFormatted )|| ((calendarFrom.getMonth() !== month)) ) ? true : false;

                        }else{
                            muted = (calendarFrom.getMonth() !== month);
                        }
                        currentWeek.push({
                            date: new Date(calendarFrom),
                            muted: muted,
                            selected: isSelected
                        });

                        calendarFrom.setDate(calendarFrom.getDate() + 1);

                        if (calendarFrom.getDay() === 1) {
                            weeks.push(currentWeek);
                            currentWeek = [];
                        }
                    }
                    return weeks;
                };

                // ------------------------------
                // Private Method's
                // ------------------------------
                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {
                        vm.isValidityValid = true;

                        $scope.ngModel = vm.isValidityValid ? value : null;
                    }
                };

                vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                vm.dayNames = $locale.DATETIME_FORMATS.SHORTDAY;
                vm.monthNames = $locale.DATETIME_FORMATS.MONTH;
                vm.years = [];

                var currentYear = chosenDate.getFullYear();
                for (var i = 0; i < 65; i++) {
                    vm.years.push(currentYear - i);
                }
                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                vm.nextMonth = function() {
                    vm.setMonth(vm.currentDate.getMonth() + 1);
                };
                vm.previousMonth = function() {
                    vm.setMonth(vm.currentDate.getMonth() - 1);
                };
                vm.setMonth = function(month) {
                    vm.currentDate.setMonth(month);
                    vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                    vm.pickerYear = vm.currentDate.getFullYear();
                };
                vm.setYear = function(year) {
                    vm.currentDate.setFullYear(year);
                    vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                    vm.pickerYear = vm.currentDate.getFullYear();
                };
                vm.setDate = function(newDate) {
                    var parsedDate = parseDate(newDate);
                    chosenDate = parsedDate;
                    vm.currentDate = parsedDate;
                    vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                    vm.pickerYear = vm.currentDate.getFullYear();
                };
                vm.selectDate = function(date) {

                    writeValue(date);

                    var handler = $scope.onChangeDate();
                    if (handler) {
                        handler(date);
                    }
                };

                // ------------------------------
                // Event's
                // ------------------------------
            }]
        };
    }]);;angular.module('uk-toolkit.components')
    .directive('ukChipsInput', ['$compile', function($compile) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@', //Place Holder for the input
                onSearch: '&', //When Call to Search
                onItemSelected: '&', //Last chance to modify the model
                itemText: '@', // Text Value Display
                ngBlur: '&', //Call when the user out of the focus (BLUR)
                minLengthForSearch: '@', // Minimum size of characters for start search
                delayTimeForSearch: '@' //Delay Time for start search (milliseconds)
            },
            transclude: false,
            templateUrl: 'uk-chips-input/uk-chips-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.chips = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: ['$scope', '$element', '$q', '$timeout', function($scope, $element, $q, $timeout) {
                var container = $element.find("uk-chips-input-container");
                var autoCompleteContainer = $element.find("uk-input-autocomplete");
                var resultList = autoCompleteContainer.find("uk-dropdown");
                var selectedItems = container.find("uk-chips-wrap");

                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true
                };

                try{
                    $scope.minLengthForSearch = parseInt($scope.minLengthForSearch); 
                }catch(error){
                    $scope.minLengthForSearch = 3;
                }
                

                try{
                    $scope.delayTimeForSearch = parseInt($scope.delayTimeForSearch);
                }catch(error){                    
                    $scope.delayTimeForSearch = 650; 
                }
                

                //---------------------------------------------------
                // Build the selected chips, by his template!
                var template = null;
                if ($scope.template) {
                    template = $scope.template;
                } else {
                    template = '{{item.' + $scope.itemText + '}}';
                }

                var fragment = [
                    '<uk-chips-selecteds class="tc-text-light">',
                    '   <uk-chip ng-repeat="item in ngModel" ng-class="{\'for-delete\': item.forDelete}">',
                    '       <uk-chip-content>',
                    template,
                    '       </uk-chip-content>',
                    '       <uk-chip-remove ng-click="vm.removeFromModel($index)">',
                    '           <div class="uk-icon-close">',
                    '           </div>',
                    '       </uk-chip-remove>',
                    '   </uk-chip>',
                    '</uk-chips-selecteds>'
                ].join("");

                //First list
                selectedItems.html("");
                selectedItems.append($compile(fragment)($scope));
                //---------------------------------------------------


                // ------------------------------
                // Private Method's
                // ------------------------------
                var AddToModel = function(item) {
                    var handler = $scope.onItemSelected();
                    if (handler) {
                        handler(item).then(function(newItem) {
                            item = newItem;
                        });
                    }


                    //Find if exists =), to check unique (only for item-text,
                    // otherwise, they need to do in the "on-selected-item" event)!
                    if ($scope.ngModel && item) {
                        var customFilter = {};
                        customFilter[$scope.itemText] = item[$scope.itemText];

                        var exists = _.find($scope.ngModel, customFilter);
                        if (exists) {
                            //Nullify item to skip the add phase to Model
                            item = null;
                        }
                    }

                    if (item) {
                        if (!$scope.ngModel) {
                            $scope.ngModel = [];
                        }

                        $scope.ngModel.push(item);

                        vm.isValidityValid = true;
                    }

                    //Reset the counters!
                    delete vm.resultCollection; //Result Collection for Autocomplete
                    delete vm.selectedResultCollection;
                    vm.viewValue = "";
                    autoCompleteContainer.removeClass("uk-open");
                };

                var clearDeletionMarker = function() {
                    //Remove the last for-delete (special UI marker)
                    if ($scope.ngModel && $scope.ngModel.length > 0) {
                        delete $scope.ngModel[$scope.ngModel.length - 1].forDelete;
                    }
                };
                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.removeFromModel = function(index) {
                    if ($scope.ngModel) {
                        $scope.ngModel.splice(index, 1);

                        if ($scope.ngModel.length === 0) {
                            $scope.ngModel = null;
                            vm.isValidityValid = true;
                        }
                    }
                };
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    return ($scope.ngModel && $scope.ngModel.length > 0);
                };

                // ------------------------------
                // Event's
                // ------------------------------
                vm.onKeyDown = function(ev) {
                    switch (ev.keyCode) {
                        case 8: //DELETE
                            if (vm.viewValue === "" && $scope.ngModel && $scope.ngModel.length > 0) {
                                //get the last model!
                                var lastIndex = $scope.ngModel.length - 1;
                                var lastModel = $scope.ngModel[lastIndex];
                                if (lastModel.forDelete) {
                                    vm.removeFromModel(lastIndex);
                                } else {
                                    lastModel.forDelete = true;
                                }
                            }
                            break;
                    }
                };

                vm.onKeyUp = function(ev) {

                    switch (ev.keyCode) {
                        case 8:
                            //DO NOTHING (FOR NO-BLOCK KEYDOWN)
                            break;
                        case 13: //ENTER
                            if (vm.resultCollection) {
                                vm.selectResultItem(vm.selectedResultCollection);
                            } else {
                                //If on-search is empty, means, free text
                                var handler = $scope.onSearch();
                                if (!handler) {
                                    AddToModel({
                                        name: vm.viewValue
                                    });
                                }
                            }
                            clearDeletionMarker();
                            break;
                        case 38: //UP ARROW
                            if (vm.resultCollection) {
                                vm.selectedResultCollection--;
                                if (vm.selectedResultCollection < 0) {
                                    vm.selectedResultCollection = vm.resultCollection.length - 1;
                                }
                            }
                            clearDeletionMarker();
                            break;
                        case 40: //DOWN ARROW
                            if (vm.resultCollection) {
                                vm.selectedResultCollection++;
                                if (vm.selectedResultCollection >= vm.resultCollection.length) {
                                    vm.selectedResultCollection = 0;
                                }
                            }
                            clearDeletionMarker();
                            break;
                        default:
                            clearDeletionMarker();
                            break;
                    }
                };
                vm.selectResultItem = function(index) {
                    if (vm.resultCollection) {
                        var selected = vm.resultCollection[index];
                        AddToModel(selected);
                    }
                };
                vm.onResultsItemHover = function(ev, index) {
                    if (vm.resultCollection) {
                        vm.selectedResultCollection = index;
                    }
                };
                vm.onFocus = function() {
                    container.addClass("uk-input-focus");
                };
                vm.onBlur = function() {
                    container.removeClass("uk-input-focus");
                    clearDeletionMarker();
                    var handler = $scope.ngBlur();
                    if (handler) {
                        handler();
                    }
                    // AA: Se limpia el input de busqueda
                    vm.viewValue = null;
                    // AA: Se deja un delay para cerrar la lista de items para no chocar con un posible onClick
                    var delay = $timeout(function(){
                        autoCompleteContainer.removeClass("uk-open");
                        },120);
                };
                var debounced = null;
                vm.onChange = function(value) {
                    if (value && value.length >= parseInt($scope.minLengthForSearch)) {
                        if (!debounced) {
                            debounced = _.debounce(function() {
                                var handler = $scope.onSearch();
                                if (handler) {
                                    var defer = handler(vm.viewValue);
                                    defer.then(function(results) {
                                        
                                        // AA: Si no se encuentran resultados no debe pintar nada
                                        if(results.length === 0){
                                            autoCompleteContainer.removeClass("uk-open");
                                            return;
                                        }

                                        //---------------------------------------------------
                                        // Bind Template Html
                                        var template = null;
                                        if ($scope.template) {
                                            template = $scope.template;
                                        } else {
                                            template = '<a>{{item.' + $scope.itemText + '}}</a>';
                                        }

                                        var fragment = [
                                            '<ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results">',
                                            "<li ng-repeat='item in vm.resultCollection' ng-click='vm.selectResultItem($index)' ng-mouseover='vm.onResultsItemHover($event,$index)' ng-class='{\"uk-active\":vm.selectedResultCollection == $index}'>",
                                            template,
                                            '</li>',
                                            '</ul>'
                                        ].join("");

                                        //First list
                                        vm.resultCollection = results;
                                        vm.selectedResultCollection = 0;
                                        resultList.html("");
                                        resultList.append($compile(fragment)($scope));
                                        //---------------------------------------------------

                                        autoCompleteContainer.addClass("uk-open");
                                    });
                                } else {
                                    //Add directly to Model
                                    $scope.itemText = "name"; //set the name
                                }
                            }, parseInt($scope.delayTimeForSearch));
                        }
                        debounced();

                    } else {
                        if (debounced) {
                            debounced.cancel();
                            debounced = null;
                        }
                        delete vm.resultCollection; //Result Collection for Autocomplete
                        delete vm.selectedResultCollection;
                        autoCompleteContainer.removeClass("uk-open");
                    }
                };
            }]
        };
    }]);
;angular.module('uk-toolkit.components')
    .directive('ukDatepickerInput', ['$filter', '$window', function($filter, $window) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                dateFormat: '@',
                startDate:'=',
                minDate :'=',
                ngChange: '&', //Call when the date of the calendar changed!
                ngBlur: '&' //Call when the user out of the focus (BLUR)

            },
            transclude: false,
            templateUrl: 'uk-datepicker-input/uk-datepicker-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.isDate = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: ['$scope', '$element', '$q', function($scope, $element, $q) {
                var container = $element.find("material-input");
                var windowDOM = window.angular.element($window);
                var focusActiveClass = "uk-focus-active";

                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true,
                    checkFocus: false
                };



                vm.startDate = ($scope.startDate) ? $scope.startDate : null;
                vm.minDate = ($scope.minDate) ? $scope.minDate : null;
                
                // ------------------------------
                // Private Method's
                // ------------------------------
                var writeValue = function(value) {

                    var format = ($scope.dateFormat || 'dd-MM-yyyy');

                    if (value !== null && value !== undefined) {
                        vm.isValidityValid = true;
                        var tomorrowDate = new Date();
                        /*tomorrowDate.setDate(tomorrowDate.getDate() + 1);
                        date1 = Date.UTC(value.getFullYear(), value.getMonth(), value.getDate());
                        date2 = Date.UTC(tomorrowDate.getFullYear(), tomorrowDate.getMonth(), tomorrowDate.getDate());
                        if ((date1 - date2) < 0) {
                            value = tomorrowDate;
                        }*/

                        $scope.ngModel = vm.isValidityValid ? value : null;

                        
                        vm.viewValue = $filter('date')(value, format);
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                var closeCalendar = function() {
                    container.removeClass("uk-open uk-datepicker-focus");
                };
                windowDOM.on('click', closeCalendar);

                vm.cancelPropagation = function(ev) {
                    ev.stopPropagation();
                    ev.preventDefault();
                };
                vm.onFocus = function() {
                    vm.checkFocus = true;
                    $element.addClass(focusActiveClass);
                    angular.element(document.querySelectorAll('.uk-open')).removeClass('uk-open uk-datepicker-focus');

                    container.addClass("uk-open uk-datepicker-focus ");

                    if (vm.isValidityValid) {
                        //vm.viewValue = $scope.ngModel;
                    }
                };
                vm.onBlur = function() {
                    vm.checkFocus = true;
                    $element.removeClass(focusActiveClass);
                    if (vm.isValidityValid) {

                    }

                    var handler = $scope.ngBlur();
                    if (handler && vm.checkFocus) {
                        handler();
                    }
                    vm.checkFocus = false;
                };
                vm.onChangeDate = function(date) {
                    writeValue(date);

                    closeCalendar();

                     var handler = $scope.ngChange();
                    if (handler) {
                        handler(date);
                    }
                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);

                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });


                // ------------------------------
                // Destroy
                // ------------------------------
                $scope.$on("$destroy", function() {
                    windowDOM.off('click', closeCalendar);
                });
            }]
        };
    }]);;angular.module('uk-toolkit.components')
    .directive('ukEmailInput', ['$Localization', function($Localization) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                maxLength: '@',
                errorResource: '@'
            },
            transclude: false,
            templateUrl: 'uk-email-input/uk-email-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.email = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: ['$scope', '$element', '$q', function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var focusActiveClass = "uk-focus-active";
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true
                };

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                var isEmail = function(raw) {
                    //VALIDATE EMAIL 
                    var value = (raw || '');
                    if (value.length === 0) {
                        return true;
                    }
                    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return (regex.test(value));
                };


                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {
                        vm.isValidityValid = isEmail(value);

                        // Only set the value if pass the validation!
                        $scope.ngModel = vm.isValidityValid ? value.toLowerCase() : null;
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                vm.onFocus = function() {
                    $element.addClass(focusActiveClass);
                };
                vm.onBlur = function() {
                    $element.removeClass(focusActiveClass);
                    if ($scope.ngModel) {
                        vm.viewValue = $scope.ngModel;
                    }
                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);
                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });

            }]
        };
    }]);;angular.module('uk-toolkit.components')
    .directive('ukLoading', ['$filter', '$Localization', '$timeout', function($filter, $Localization, $timeout) {
        return {
            restrict: 'E',
            scope: {
                legend: '@'
            },
            transclude: false,
            templateUrl: 'uk-loading/uk-loading.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
            },
            controller: ['$scope', '$element', '$q', function($scope, $element, $q) {
            }]
        };
    }]);
;angular.module('uk-toolkit.components')
    .directive('ukNumberInput', ['$filter', '$Localization', function($filter, $Localization) {
        return {
            restrict: 'E',
            require: ['ukNumberInput', 'ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                minValue: '@',
                maxValue: '@',
                maxLength: '@',
                format: '=?',
                errorResource: '@',
                ngDisabled:'=',
                ngChange:'&'
            },
            transclude: false,
            templateUrl: 'uk-number-input/uk-number-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                var ngModel = ctrls[1];
                var vm = scope.vm;

                // do nothing if no ng-model
                if (!ngModel) {
                    return;
                }

                // ------------------------------
                // Event's
                // ------------------------------
                scope.onFocus = function() {
                    //$element.addClass(focusActiveClass);
                    if (vm.isValidityValid) {
                        vm.viewValue = scope.ngModel;
                    }
                };
                scope.onBlur = function() {
                    //$element.removeClass(focusActiveClass);
                    ctrl.humanizeViewValue();
                };
                scope.onChange = function(value) {
                    ngModel.$setViewValue(value);
                     if(scope.ngChange){
                     scope.ngChange();   
                    }
                };

                // ------------------------------
                // ngModel Methods
                // ------------------------------
                // This functions only "call" when ngModel changes from outside ^^ the directive, 
                // soooo before we put that value we try to parse and format as expected
                // IN RESUME, THIS FORMATTERS ONLY CALL IF THE NGMODEL CHANGE FROM
                // OUTSIDE THE DIRECTIVE :P!
                ngModel.$formatters.push(function(inputValue) {
                    //always set the vm.viewValue
                    vm.viewValue = inputValue;
                    ngModel.$setViewValue(inputValue); //Init $parser and $validator process
                    ctrl.humanizeViewValue();

                    return inputValue;
                });

                // Add the parser , to ensure the model will be the "real model value"
                // CALL FIRST, if fails, all stop, and set invalid the "directive"
                ngModel.$parsers.push(ctrl.tryToParseViewValue);

                // Add the validators (call always , but wee only change the value programaticaly)
                // IF PARSER IS SUCCESSFULLY , CALL THE VALIDATORS 
                // (can be a multiple validations :P)
                ngModel.$validators.isNumber = function(modelValue, viewValue) {
                    return vm.isValidityValid;
                };
            },
            controller: ['$scope', '$element', '$q', function($scope, $element, $q) {
                var ctrl = this;

                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    isValidityValid: true
                };

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------

                // VALIDATOR: Check if the value is Number
                this.isNumber = function(rawValue) {
                    var value = rawValue;

                    //VALIDATE IF IS NUMBER
                    if (!value || (value && value.length === 0)) {
                        vm.isValidityValid = true;
                        return vm.isValidityValid;
                    }
                    var isNumber = (/^-?\d+$/.test(value)) ? true : undefined;

                    vm.isValidityValid = isNumber; //set for better perfomance
                    return vm.isValidityValid;
                };

                // VALIDATOR: Check if the value is in Range
                this.isInRange = function(rawValue) {
                    //Assume number, because isNumber called first
                    var numberValue = parseInt(rawValue);

                    var maxValue = parseInt($scope.maxValue);
                    var minValue = parseInt($scope.minValue);

                    maxValue = isNaN(maxValue) ? Number.MAX_VALUE : maxValue;
                    minValue = isNaN(minValue) ? -(Math.pow(2, 53) - 1) : minValue;

                    var isInMinRange = true;
                    var isInMaxRange = true;

                    if (numberValue < minValue) {
                        isInMinRange = false;
                    }

                    if (numberValue > maxValue) {
                        isInMaxRange = false;
                    }

                    if (!isInMinRange || !isInMaxRange) {
                        vm.isValidityValid = false; //not in range
                    }

                    return vm.isValidityValid;
                };

                // ------------------------------
                // Try to parse the Raw Value to "Model Value"
                // (if fails the pipeline is cancelled and set invalid)
                // ------------------------------
                this.tryToParseViewValue = function(rawViewValue) {
                    if (ctrl.isNumber(rawViewValue) && ctrl.isInRange(rawViewValue)) {
                        var modelValue = parseInt(rawViewValue);
                        return modelValue;
                    }
                };

                // ------------------------------
                // Parse the Model Value to a "Friendly Value"
                // for the user
                // ------------------------------
                this.humanizeViewValue = function() {
                    if (vm.isValidityValid) {
                        if ($scope.format === true) {
                            vm.viewValue = $filter('number')($scope.ngModel, 0);
                        }
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                //Check character input is only number
                characterNumber = function(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false;
                    }
                    return true;
                };

                vm.isValid = function() {
                    return vm.isValidityValid;
                };
                vm.isFilled = function() {
                    return (vm.viewValue || '').toString().length > 0;
                };
            }]
        };
    }]);;angular.module('uk-toolkit.components')
    .directive('ukOptionItems', ['$compile', function($compile) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            template: '',
            scope: {
                ngModel: '=', // Ng-Model
                itemCollection: '=',
                optionTemplate: '@',
                optionChange: '&'
            },
            compile: function() {
                return {             
                    pre: function(scope, element, attrs, controller, transcludeFn) {


                        //---------------------------------------------------
                        // Bind Template Html
                        var fragment = [
                            '<div ng-repeat="item in itemCollection">',
                            '   <a href="" ng-click="select(item)" class="uk-button uk-button-large uk-button-radio" ng-class="{\'uk-active\': isEqual(item)}">',
                            scope.optionTemplate,
                            '   </a>',
                            '</div>'
                        ].join("");

                        element.append($compile(fragment)(scope));
                        //---------------------------------------------------


                    }         
                };
            },
            controller: ['$scope', '$element', '$compile', function($scope, $element, $compile) {

                //---------------------------------------------------
                // Bind Template Html
                $scope.select = function(item) {
                    $scope.ngModel = item;

                    var handler = $scope.optionChange();
                    if (handler) {
                        handler(item);
                    }
                };
                //---------------------------------------------------

                $scope.isEqual = function(itemToCompare) {
                    return angular.equals($scope.ngModel, itemToCompare);
                };

            }]
        };
    }]);;angular.module('uk-toolkit.components')
    .directive('ukOptionList', ['$filter', '$Localization', function($filter, $Localization) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                errorResource: '@',
                ngChange: '&',
                itemCollection: '='
            },
            transclude: true,
            templateUrl: 'uk-option-list/uk-option-list.tpl.html',
            compile: function() {
                return {             
                    post: function(scope, element, attrs, controller, transcludeFn) {         

                        //Check Items Exists
                        if (!angular.isDefined(attrs.itemCollection)) {
                            throw {
                                error: "ITEM_COLLECTION_NOT_DEFINED_IN_OPTION_LIST"
                            };
                        }

                        //Not render yet from angularJS , so , transform into a Comment Node
                        var templates = element.find("option-template");
                        //templates.html("<!--" + templates.html().trim() + "-->");

                        scope.vm.template = templates.html().trim();
                    }              
                };
            },
            controller: ['$scope', '$element', '$q', '$timeout', function($scope, $element, $q,$timeout) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true
                };

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                // ------------------------------
                // Exposed Method's
                // ------------------------------
                
                vm.optionChange = function(item) {
                    $scope.ngModel = item;

                    if ($scope.ngChange) {
                        //CALL ON-CHANGE BIND
                        var delay = $timeout(function() {
                            $timeout.cancel(delay);
                            $scope.ngChange();
                        }, 0);

                    }
                };
                // ------------------------------
                // Event's
                // ------------------------------


            }]
        };
    }]);
;angular.module('uk-toolkit.components')
    .directive('ukPhoneInput', ['$filter', '$Localization', function($filter, $Localization) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                country: '@',
                errorResource: '@'
            },
            transclude: false,
            templateUrl: 'uk-phone-input/uk-phone-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.text = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: ['$scope', '$element', '$q', function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var focusActiveClass = "uk-focus-active";
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true,
                    phoneLength: 9
                };
                $scope.format = $scope.format !== "false" ? true : false;

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                var isPhoneOrOfuscatePhone = function(raw) {
                    //VALIDATE EMAIL
                    var value = (raw || '');
                    if (value.length === 0) {
                        return true;
                    }
                    return (/^[0-9-*]+$/.test(value));
                };

                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {
                        var cleanPhone = (value + "").replace(/\ /ig, '');
                        var _isPhone = isPhoneOrOfuscatePhone(cleanPhone);
                        var fullyValid = false;
                        var phoneValue = cleanPhone;

                        if (_isPhone) {

                            var minLength = 9;
                            var maxLength = 9;
                            var isInMinLength = true;
                            var isInMaxLength = true;

                            if (cleanPhone.toString().length < minLength) {
                                isInMinLength = false;
                            }

                            if (cleanPhone.toString().length > maxLength) {
                                isInMaxLength = false;
                            }

                            if (isInMinLength && isInMaxLength) {
                                fullyValid = true; // All validation pass!
                            }
                        }

                        vm.isValidityValid = fullyValid;

                        $scope.ngModel = vm.isValidityValid ? phoneValue : null;
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                //Check character input is only number
                characterNumber = function(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false;
                    }
                    return true;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                vm.onKeyUp = function(ev, thisObject) {
                    switch (ev.keyCode) {
                        case 187: //*
                            if (thisObject.vm.viewValue.length < 2) {
                                thisObject.vm.viewValue = "";
                            } else {
                                thisObject.vm.viewValue = thisObject.vm.viewValue.substring(0, thisObject.vm.viewValue.length - 1);
                                break;
                            }
                    }
                };

                vm.onFocus = function() {
                    vm.phoneLength = 9;
                    $element.addClass(focusActiveClass);
                    if (vm.isValidityValid) {
                        vm.viewValue = $scope.ngModel;
                    }
                };
                vm.onBlur = function() {
                    vm.phoneLength = 11;
                    $element.removeClass(focusActiveClass);
                    if (vm.isValidityValid && $scope.ngModel) {
                        vm.viewValue = $scope.ngModel.toString().substring(0, 1) + ' ' + $scope.ngModel.toString().substring(1, 5) + ' ' + $scope.ngModel.toString().substring(5, 9);
                    }
                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);

                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });
            }]
        };
    }]);;angular.module('uk-toolkit.components')
    .directive('ukRutInput', ['$filter', '$Localization', '$timeout', function($filter, $Localization, $timeout) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                errorResource: '@',
                ngChange: '&'
            },
            transclude: false,
            templateUrl: 'uk-rut-input/uk-rut-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.rut = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: ['$scope', '$element', '$q', function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var focusActiveClass = "uk-focus-active";
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true,
                    rutLength: 12
                };

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                var calculateDv = function(run) {
                    var M = 0;
                    var S = 1;
                    var T = run;
                    for (; T; T = Math.floor(T / 10)) {
                        S = (S + T % 10 * (9 - M++ % 6)) % 11;
                    }
                    return (S ? S - 1 : 'k').toString();
                };
                var isRut = function(raw) {
                    var rut = (raw || '');
                    if (rut.length === 0) {
                        return true;
                    }

                    if (rut && rut.toString().length < 7) {
                        return false;
                    }

                    var cleanRut = rut.replace(/\./ig, '').replace(/-/ig, '');
                    if (!(/^[0-9]+[0-9kK]{1}$/.test(cleanRut))) {
                        return false;
                    }

                    var dv = cleanRut.substring(cleanRut.length - 1, cleanRut.length).toLowerCase();
                    var run = cleanRut.substring(0, cleanRut.length - 1);

                    return (calculateDv(run) === dv.toString());
                };

                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {

                        vm.isValidityValid = isRut(value);
                        $scope.ngModel = vm.isValidityValid ? (value.replace(/\./ig, '').replace(/-/ig, '')).toUpperCase() : null;

                        if (vm.isValidityValid) {
                            vm.trigger("ngChange", value);
                        }
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.trigger = function(eventName) {
                    var delay = $timeout(function() {
                        $timeout.cancel(delay);

                        //call handler
                        $scope[eventName]();

                    }, 10);

                };

                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                vm.onFocus = function() {
                    vm.rutLength = 12;
                    $element.addClass(focusActiveClass);
                    if (vm.isValidityValid) {
                        vm.viewValue = $scope.ngModel;
                    }
                };
                vm.onBlur = function(value) {
                    vm.rutLength = 12;
                    $element.removeClass(focusActiveClass);
                    if (vm.isValidityValid && $scope.ngModel) {

                        // Get DV And Run in parts
                        var fullRut = $scope.ngModel;
                        var dv = fullRut.substring(fullRut.length - 1, fullRut.length).toLowerCase();
                        var run = fullRut.substring(0, fullRut.length - 1);

                        vm.viewValue = "{0}-{1}".format([
                            $filter('number')(run, 0),
                            dv
                        ]);
                    }
                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);

                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });
            }]
        };
    }]);;angular.module('uk-toolkit.components')
    .directive('ukSelectInput', ['$filter', '$Localization', '$interpolate', '$compile', function($filter, $Localization, $interpolate, $compile) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                errorResource: '@',
                itemCollection: '&',
                items: '=',
                emptyItemText: '@',
                loading: '@',
                loadingItemText: '@',
                ngChange: '&',
                itemText: '@', // Template for the "selected value" && "list value"
                ngDisabled: '=' // Flag used to determinate if the control must be disabled or not.
            },
            transclude: true,
            templateUrl: 'uk-select-input/uk-select-input.tpl.html',
            controller: ['$scope', '$element', '$q', '$timeout', function($scope, $element, $q, $timeout) {
                var itemsNode = $element.find("select-container");
                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isLoading: true,
                    isValidityValid: true
                };

                var childScope = $scope.$new();

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                var resolveItemsDefer = $q.defer();

                //Check Data if Array or Promise
                var collection = $scope.itemCollection();
                if (collection && angular.isArray(collection)) {
                    //Smooth Delay
                    var delay = $timeout(function() {
                        $timeout.cancel(delay);
                        resolveItemsDefer.resolve(collection);
                    }, 350);
                } else if (collection && collection.then) {
                    //is Promise
                    collection.then(function(collection) {
                        resolveItemsDefer.resolve(collection);
                    }, function(err) {
                        resolveItemsDefer.reject();
                    });
                } else if (collection) {
                    throw {
                        error: "ITEM_COLLECTION: ITEMS_MUST_BE_ARRAY_OR_PROMISE"
                    };
                } else {
                    vm.isLoading = false;
                }

                var fill = function(values) {
                    var fragment = [
                        '<select ng-model="selectValue" ng-disabled="ngDisabled" ng-change="vm.selectChange(selectValue)" ',
                        'ng-options="selecteItem.' + $scope.itemText + ' for selecteItem in items">',
                        '</select>'
                    ].join("");
                    childScope.items = values;
                    childScope.selectValue = {};

                    var compiledDOM = $compile(fragment)(childScope);
                    itemsNode.html('').append(compiledDOM);

                    vm.isLoading = false;

                };
                resolveItemsDefer.promise.then(fill);


                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.selectChange = function(item, unMute) {
                    if (item) {
                        //Load When change (Post Compilation)
                        vm.displayValue = $interpolate("{{" + $scope.itemText + "}}")(item);
                        $scope.ngModel = item; //SET THE ITEM IN THE MODEL

                        if (!unMute) {
                            if ($scope.ngChange) {
                                //CALL ON-CHANGE BIND
                                var delay = $timeout(function() {
                                    $timeout.cancel(delay);
                                    $scope.ngChange();
                                }, 0);

                            }

                        }
                    }
                };


                $scope.$watch("items", function(value) {
                    if (value) {


                        if (typeof _.find(value, $scope.ngModel) === 'undefined' || typeof $scope.ngModel === 'undefined') {

                            $scope.ngModel = null;
                            vm.displayValue = null;

                        }

                        fill(value);


                    }
                });

                $scope.$watch("loading", function(value) {

                    if (value !== undefined) {
                        if (value === "true") {

                            $scope.ngModel = null;
                            vm.displayValue = null;
                            vm.isLoading = true;
                            $scope.ngDisabled=true;

                        } else {
                            vm.isLoading = false;
                            $scope.ngDisabled=false;
                        }
                    }

                });

                $scope.$watch("ngModel", function(newValue, oldValue) {
                    if (newValue !== oldValue && newValue !== null && newValue !== undefined) {
                        if (childScope.selectValue !== newValue) {
                            vm.selectChange(newValue, true);
                            childScope.selectValue = newValue;
                        }
                    } else {

                        vm.displayValue = "";

                        $scope.ngModel = null;
                    }
                });

            }]
        };
    }]);
;angular.module('uk-toolkit.components')
    .directive('ukTextInput', ['$Localization', '$compile', function($Localization,$compile) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                restricted: '@',
                errorResource: '@', 
                maxLength  : '=', 
                patternModel : '@', 
                isLoading:'=',
                ngDisabled:'='
            },
            transclude: false,
            templateUrl: 'uk-text-input/uk-text-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                if (scope.patternModel) { 
                    elm.find("input").attr("ng-pattern", scope.patternModel); 
                    $compile(elm.contents())(scope); 
 
                } 
                ctrl.$validators.text = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: ['$scope', '$element', '$q', function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var focusActiveClass = "uk-focus-active";
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true
                };

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                var isValidText = function(raw) {
                    //VALIDATE TEXT 
                    var value = (raw || '');
                     if (value.length === 0 || $scope.restricted === undefined) {
                        return true;
                    }else{

                         return (/^[A-Za-z\u00E0-\u00FC-' ]+$/i.test(value));
                    }
                };


                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {
                        vm.isValidityValid = isValidText(value);

                        // Only set the value if pass the validation!
                        $scope.ngModel = vm.isValidityValid ? value.toUpperCase() : null;
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                vm.onFocus = function() {
                    $element.addClass(focusActiveClass);
                };
                vm.onBlur = function() {
                    $element.removeClass(focusActiveClass);
                    vm.viewValue = $scope.ngModel;
                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);

                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });

            }]
        };
    }]);